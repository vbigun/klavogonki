const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');

require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

// app.get('/chat', /*passport.authenticate('jwt'),*/ function (req, res) {
//   res.sendFile(path.join(__dirname, 'chat.html'));
// });

app.get('/login', function (req, res) {
  res.sendFile(path.join(__dirname, 'login.html'));
});

app.get('/race', /*passport.authenticate('jwt'),*/ function (req, res) {
  res.sendFile(path.join(__dirname, 'race.html'));
});

app.post('/login', function (req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.email === userFromReq.email);
  // const nameInDB = users.find(name => name.login === userFromReq.login);
  // console.log(nameInDB);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret', { expiresIn: '24h' });
    res.status(200).json({ auth: true, token });
    // io.connect();
  } else {
    res.status(401).json({ auth: false });
  }
});


let activeUsers = [];
let disconnectUsers = [];
let gameTimer = 5;

io.on('connection', socket => {

  socket.on('login', payload => {
    const { token } = payload;
    const email = jwt.decode(token).email;

   

    // const interval = setInterval(() => { 
    //   io.sockets.emit('game', { time: gameTimer });
    //   gameTimer--;
    //   if(gameTimer === -1) {
    //     clearInterval(interval);
    //     gameTimer = 5;
    //   }
    // }, 1000);

    if (activeUsers.map(x => x.user.email).includes(email)) {
      console.log('user already playing');
      socket.emit('logout', {})
      socket.disconnect();
    } else {
      console.log('new user');
      const json = {
        socket: socket,
        user: jwt.decode(token),
        token: token,
        score: 0
      }
      activeUsers.push(json);
      socket.broadcast.emit('newUsers', { users: activeUsers.map(x => x.user.email) });
      socket.emit('newUsers', { users: activeUsers.map(x => x.user.email) });
    }

    // console.log(activeUsers.length > 1);
    if(activeUsers.length > 1) {
      let interval = setInterval(() => { 
        io.sockets.emit('game', { time: gameTimer });
        gameTimer--;
        if(gameTimer === -1) {
          clearInterval(interval);
          // alert('Finish');

          
          // activeUsers.sort((a, b) => a.score > b.score); 

          let bubbleSort = (activeUsers) => {
            console.log(activeUsers);
            let len = activeUsers.length;
            for (let i = 0; i < len; i++) {
                for (let j = 0; j < len; j++) {
                    if (activeUsers[j + 1] && activeUsers[j].score < activeUsers[j + 1].score) {
                        let tmp = activeUsers[j];
                        activeUsers[j] = activeUsers[j + 1];
                        activeUsers[j + 1] = tmp;
                    }
                }
            }
            return activeUsers;
        };

        const sortedUsers = bubbleSort(activeUsers)

        io.sockets.emit('endGame', { results: sortedUsers.map(x => { return { email: x.user.email, score: x.score } }) });
          
          gameTimer = 5;
        }
      }, 1000);
    }
        // socket.broadcast.emit('newUser', {login: userLogin });
  });

  socket.on('update', payload => {
    const x = activeUsers.filter(a => a.token === payload.token)[0];
    const userToUpdate = x.user;
    const token = x.token;
    activeUsers.filter(u => u.token === payload.token)[0].score = payload.score;
    socket.emit('updateScore', { email: userToUpdate.email, score: payload.score });
    socket.broadcast.emit('updateScore', { email: userToUpdate.email, score: payload.score });
  });

  socket.on('disconnect', payload => {
    console.log('disconnecting...')
    activeUsers = activeUsers.filter(json => json.socket != socket);
    console.log(activeUsers);
    socket.broadcast.emit('newUsers', { users: activeUsers.map(x => x.user.email) });
    socket.emit('newUsers', { users: activeUsers.map(x => x.user.email) });
  });

  
});