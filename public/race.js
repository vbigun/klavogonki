window.onload = () => {
    // const users = require('../user.json');

    let login = '';

    const jwt = localStorage.getItem('jwt');

    if(!jwt) {
        location.replace('/login');
    } else {
        const titleStart = document.querySelector('.title-start');
        const finalTitle = document.querySelector('.final-title');
        let arr = [];
        let enteredWords = [];
        const socket = io.connect('ws://localhost:3000');
        const text = document.querySelector('.text');
        const textInput = document.querySelector('.write-text');
        const finalScore = document.getElementById('finalScore');
        let currentWordId = 0;

        finalTitle.style.display = 'none';

        textInput.addEventListener('keydown', checkSymbols);
        textInput.addEventListener('keydown', calculateSymbols)

        socket.emit('login', { token: jwt });
        const userList = document.querySelector('.user-list');

        // socket.on('game', payload => {
        //     const time = payload.time;
        //     console.log(time);
        //     document.getElementById('timer').innerHTML = time;
        // });

        socket.on('logout', payload => {
            location.replace('/login');
        });

        socket.on('endGame', payload => {
            
            // newLi.innerHTML = `${payload.results.map(a => a.email)} - ${payload.results.map(a => a.score)}`;
            finalScore.innerHTML =  payload.results.map((u) => `<li>${u.email} - ${u.score}</li>`).join(' ');
            
            // finalScore.uppendChild(newLi);
        });

        socket.on('updateScore', payload => {
            let updateLi = document.getElementById('user-' + payload.email);
            updateLi.innerHTML = `${payload.email} - ${payload.score}`;
        });

        socket.on('newUsers', payload => {
            newUsersList =  payload.users.map((u) => `<li id="user-${u}">${u} - 0</li>`).join(' '); // payload.users.map(;
            // const newLi = document.createElement('li');
            // newLi.setAttribute('id', `user-${login}`);
            // newLi.innerHTML = `${login} - ${arr.length}`;
            userList.innerHTML = newUsersList;
            // console.log(payload.users)
            socket.on('game', payload => {
                const time = payload.time;
                console.log(newUsersList);
                document.getElementById('timer').innerHTML = time;
                console.log(time);
                if(time === 0) {
                    userList.style.display = 'none';
                    document.getElementById('timer').style.display = 'none';
                    text.style.display = 'none';
                    textInput.style.display = 'none';
                    titleStart.style.display = 'none';
                    finalTitle.style.display = 'block';
                    // let newLi = document.createElement('li');
                    // newLi.innerHTML = enteredWords;
                    // finalScore.uppendChild(newLi);
                }   
            });         
        });

        

        const textValue = text.innerHTML;

        const newText = textValue.split(" ").map((str, id) => `<span id="text-${id}">${str}</span>` ).join(' ');

        // console.log(newText);
        text.innerHTML = newText;
        document.getElementById(`text-${currentWordId}`).style.color = 'green';

        // let arr = [];
        function calculateSymbols(e) {
            // let arr = [];
            if(e.keyCode === 16 || e.keyCode === 18 || e.keyCode === 17 || e.keyCode === 8) {
                arr.pop();
            } else if (e.keyCode === 16 && e.keyCode === 18 || e.keyCode === 32) {
                !arr.push();
             } else {
                arr.push('');
            }

            if(arr.length > textValue.length) {
                textInput.disabled = true;
                alert('Stop! Text ending');
            }

            console.log(arr.length);
            // document.getElementById(`user-${login}`).innerHTML = `${login} - ${arr.length}`;
        }

        
        function checkSymbols(e) {
            
            if (e.keyCode === 32) {
                const value = textInput.value.trim();
                const rightWord = document.getElementById(`text-${currentWordId}`).innerHTML.trim();
                console.log(login);
                
                if (value === rightWord) {
                    textInput.value = '';
                    document.getElementById(`text-${currentWordId}`).style.color = 'red';
                    currentWordId++;
                    document.getElementById(`text-${currentWordId}`).style.color = 'green';
                    let json = {score: currentWordId, token: jwt}
                    socket.emit('update', json);   
                } else {
                    alert('Введи правильно текст');
                }
            }
            
            // let textValue = text.innerHTML;
            // for (let i = 0; i < textValue.length - 1 ; i++) {
            //     console.log(i++);
            // }
            
        }

    }
}